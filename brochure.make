; Drupal Brochure site make file 

; Set core version
core = 6.14

; CCK and related
projects[cck][version] = 2.6
projects[filefield][version] = 3.2
projects[imagefield][version] = 3.2
projects[imagecache][version] = 2.0-beta9
projects[imageapi][version] = 1.6

; Views and related
projects[views][version] = 2.8
projects[views_bulk_operations][version] = 1.8

; Date and related 
projects[date][version] = 2.4
projects[calendar][version] = 2.2

; Features and related
projects[strongarm][version] = 2.0-beta1
projects[features][version] = 1.0-beta4
projects[context][version] = 2.0-beta7

; Miscellaneous 
projects[ctools][version] = 1.2
projects[pathauto][version] = 1.2
projects[token][version] = 1.12
projects[vertical_tabs][version] = 1.0-beta6
projects[getid3][version] = 1.3
projects[auto_nodetitle][version] = 1.2
projects[diff][version] = 2.1-alpha3
projects[advanced_help][version] = 1.2
projects[auto_nodetitle][version] = 1.2